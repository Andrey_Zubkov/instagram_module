<?php
require (__DIR__.'/phpQuery.php');
if (!defined('_PS_VERSION_'))
  exit;
 
class MyInstagramm extends Module
{
  public function __construct()
  {
    $this->name = 'myinstagramm';
    $this->tab = 'other';
    $this->version = '1.0.0';
    $this->author = 'Zubkov Andrey';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
    $this->bootstrap = true;
 
    parent::__construct();
 
    $this->displayName = $this->l('My Instagramm');
    $this->description = $this->l('Description of my module.');
 
    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
 
    if (!Configuration::get('MYINSTAGRAMM_NAME'))      
      $this->warning = $this->l('No name provided');
  }
public function install()
{
  if (Shop::isFeatureActive())
    Shop::setContext(Shop::CONTEXT_ALL);
 
  if (!parent::install() ||
    !$this->registerHook('header') ||
    !$this->registerHook('footer') ||
    !$this->registerHook('home') ||
    !Configuration::updateValue('MYINSTAGRAMM_NAME', 'my insta')
  )
    return false;
 
  return true;
}
public function uninstall()
{
  if (!parent::uninstall() ||
    !Configuration::deleteByName('MYINSTAGRAMM_NAME')
  )
    return false;
 
  return true;
}



  public function hookHeader()
  {
  function get_html($url) {

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_CAPATH, 'cacert.pem');
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_COOKIEFILE, '');
    $html = curl_exec($ch);
    curl_close($ch);

    return $html;
  }
  function print_arr($arr){
    echo '<pre>'. print_r($arr,true) . '</pre>';
  }
  
  $url = 'https://www.instagram.com/zubkov_andrey_pr/';
  $result = get_html($url);

  //print_arr(htmlspecialchars($result));
  $doc = phpQuery::newDocument($result);
  $script = $doc->find('script');
  $text = $script->text();
  $text = strval($text); 
  $first = stripos($text,'window._sharedData = ');
  $text = substr($text, $first);
  $first = stripos($text,'{');
  $text = substr($text, $first);
  $first = stripos($text,';');
  $text = substr($text, 0, $first);
  $text=json_decode($text);
  foreach ($text->entry_data->ProfilePage[0]->user->media->nodes as $value) {
   $img[] =$value->thumbnail_src;
  }
  
  $this->context->smarty->assign('img', $img);
  return $this->display(__FILE__, 'myinstagramm.tpl');
  }

}